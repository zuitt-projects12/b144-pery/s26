// - What directive is used by Node.js in loading the modules it needs?
console.log("require()");
// - What Node.js module contains a method for server creation?
console.log("http");
// - What is the method of the http object responsible for creating a server using Node.js?
console.log("http.createServer");
// - What method of the response object allows us to set status codes and content types?
console.log("res.writeHead()");
// - Where will console.log() output its contents when run in Node.js?
console.log("In the terminal");
// - What property of the request object contains the address's endpoint?
console.log("url");