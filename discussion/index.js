// Use the require directive to load Node.js Modules
/*
	A module is a software component or part of a program that contains one or more routines.

	Example:
	"http" - lets Node.js transfer data using the HyperText transfer Protocol. It is a set of individual files that contain code ato create a "compoent" that helps establish data transfer between applications.
*/

const http = require("http"); // directive to load Node.js

/*
	Clients (browser) and servers (NodeJS, ExpressJS application) communicate by exchanging indivudal messages

	Message sent by the client, usually a web browser are called requests.
	http://home
	Message sent by the server as an answer are called responses.
*/

/*
	createServer() method - used to create an HTTP server that listens to requests
	It accepts a function and allows us to perform a certain task for our server.
*/
const port = 4000;
http.createServer((request, response) => {
	/*
		Use the writeHead() to set a status for the response - 200 means OK.

		Set the content type of the response as a plain text message.
	*/
	response.writeHead(200, {"Content-Type": "text/plain"});
	/*
		We use the response.end() method to end the response process.
	*/
	response.end("Goodbye!");
}).listen(port);

/*
	When a server is running, console will print this message.
*/

console.log(`Server running at localhost:${port}`);

/*
	A port is a virtual point wherre network connaction start and end.
	Each port is associated with a specific process or service.
	The server will be assign to port 4000 via the listen() method where the server will listen to any requests that are sent to our server.
*/

/*
	http://localhost:4000 
*/